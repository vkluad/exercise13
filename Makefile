obj-m += src/tick.o src/interv.o src/hrtimer.o src/mytimer.o

KERNELDIR := /lib/modules/$(shell uname -r)/build

all:
	$(MAKE) -C $(KERNELDIR) M=$(CURDIR) modules
clean:
	$(MAKE) -C $(KERNELDIR) M=$(CURDIR) clean

test: all
	sudo dmesg -C
	- sudo insmod src/tick.ko
	sudo insmod src/interv.ko
	sudo insmod src/hrtimer.ko
	sudo insmod src/mytimer.ko
	sleep 5
	sudo rmmod interv
	sudo rmmod hrtimer
	sudo rmmod mytimer
	sudo dmesg
